<?php

/**
 * @file
 * Provides field information for commerce_box entities.
 */

/**
 * Field base and instance definitions.
 *
 * @return  array
 *   The field bases and instances for commerce box.
 */
function commerce_box_common_field_info() {

  $field_bases = array();
  $field_instances = array();

  $field_bases['commerce_box_dimensions'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'commerce_box_dimensions',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'physical',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'physical_dimensions',
  );

  $field_instances['commerce_box_dimensions'] = array(
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'physical',
        'settings' => array(
          'field_formatter_class' => '',
        ),
        'type' => 'physical_dimensions_formatted',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'commerce_box',
    'field_name' => 'commerce_box_dimensions',
    'label' => 'Box Dimensions',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'physical',
      'settings' => array(
        'default_unit' => 'in',
        'unit_select_list' => 1,
      ),
      'type' => 'physical_dimensions_textfields',
      'weight' => 3,
    ),
  );

  $field_bases['commerce_box_max_weight'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'commerce_box_max_weight',
    'foreign keys' => array(),
    'indexes' => array(
      'weight' => array(
        0 => 'weight',
      ),
    ),
    'locked' => 0,
    'module' => 'physical',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'physical_weight',
  );

  $field_instances['commerce_box_max_weight'] = array(
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'physical',
        'settings' => array(
          'field_formatter_class' => '',
        ),
        'type' => 'physical_weight_formatted',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'commerce_box',
    'field_name' => 'commerce_box_max_weight',
    'label' => 'Box Max Weight',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'physical',
      'settings' => array(
        'default_unit' => 'lb',
        'unit_select_list' => 1,
      ),
      'type' => 'physical_weight_textfield',
      'weight' => 4,
    ),
  );

  return array(
    'field_bases' => $field_bases,
    'field_instances' => $field_instances,
  );
}
