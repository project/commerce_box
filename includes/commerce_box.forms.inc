<?php

/**
 * @file
 * Provides CRUD forms for commerce box entities.
 */

/**
 * Form callback for creating and editing commerce box entities.
 */
function commerce_box_form($form, &$form_state, $commerce_box) {
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_box') . '/includes/commerce_box.forms.inc';

  $form_state['commerce_box'] = $commerce_box;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Provide a name for this box'),
    '#default_value' => !empty($commerce_box->name) ? $commerce_box->name : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -10,
  );

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#description' => t('Select the status for this box.'),
    '#options' => commerce_box_get_statuses(),
    '#default_value' => !empty($commerce_box->status) ? $commerce_box->status : COMMERCE_BOX_STATUS_ACTIVE,
  );

  $langcode = entity_language('commerce_box', $commerce_box);

  if (empty($langcode)) {
    $langcode = LANGUAGE_NONE;
  }

  field_attach_form('commerce_box', $commerce_box, $form, $form_state, $langcode);

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 400,
  );

  // Simply use default language
  $form['language'] = array(
    '#type' => 'value',
    '#value' => $langcode,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save box'),
  );

  $form['#validate'][] = 'commerce_box_form_validate';

  return $form;
}

/**
 * Form validation handler for commerce box add/edit form.
 */
function commerce_box_form_validate(&$form, &$form_state) {
  $commerce_box = $form_state['commerce_box'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('commerce_box', $commerce_box, $form, $form_state);
}

/**
 * Form submit handler for commerce box add/edit form.
 */
function commerce_box_form_submit(&$form, &$form_state) {
  $commerce_box = &$form_state['commerce_box'];

  $commerce_box->name = $form_state['values']['name'];
  $commerce_box->status = $form_state['values']['status'];
  $commerce_box->language = $form_state['values']['language'];

  field_attach_submit('commerce_box', $commerce_box, $form, $form_state);

  commerce_box_save($commerce_box);

  drupal_set_message(t('Box saved'));
}

/**
 * Form callback for deleting commerce box entities.
 */
function commerce_box_delete_form($form, &$form_state, $commerce_box) {
  $form_state['commerce_box'] = $commerce_box;

  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_box') . '/includes/commerce_box.forms.inc';

  $content = entity_view('commerce_box', array($commerce_box->box_id => $commerce_box));

  $form = confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $commerce_box->name)),
    '',
    drupal_render($content) . '<p>' . t('Deleting this box cannot be undone') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for commerce_box_delete_form().
 */
function commerce_box_delete_form_submit($form, &$form_state) {
  $commerce_box = $form_state['commerce_box'];

  if (commerce_box_delete($commerce_box->box_id)) {
    drupal_set_message(t('%name has been deleted.', array('%name' => $commerce_box->name)));
    watchdog('commerce_box', 'Deleted box %name (SKU: @sku).', array('%name' => $commerce_box->name, '@sku' => $commerce_box->sku), WATCHDOG_NOTICE);
  }
  else {
    drupal_set_message(t('%name could not be deleted.', array('%name' => $commerce_box->name)), 'error');
  }
}
