<?php

/**
 * @file
 * Defines the inline entity form controller for Commerce Box.
 */

class CommerceBoxInlineEntityFormController extends EntityInlineEntityFormController {

  /**
   * Inline Entity Form Constructor.
   */
  public function __construct($entityType, array $settings) {
    $this->entityType = $entityType;
    $this->settings = $settings + $this->defaultSettings();

    // Convert the old box-specific variation language setting.
    if (!empty($settings['use_variation_language']) && empty($settings['override_labels'])) {
      $this->settings['override_labels'] = TRUE;
      $this->settings['label_singular'] = 'box';
      $this->settings['label_plural'] = 'boxes';
      unset($this->settings['use_variation_language']);
    }
  }

  /**
   * Overrides EntityInlineEntityFormController::tableFields().
   */
  public function tableFields($bundles) {
    $fields = array();
    $fields['name'] = array(
      'type' => 'property',
      'label' => t('Name'),
      'weight' => 1,
    );
    $fields['commerce_box_dimensions'] = array(
      'type' => 'field',
      'label' => t('Dimensions'),
      'weight' => 2,
    );
    $fields['commerce_box_max_weight'] = array(
      'type' => 'field',
      'label' => t('Max Weight'),
      'weight' => 3,
    );
    $fields['status'] = array(
      'type' => 'property',
      'label' => t('Status'),
      'weight' => 4,
    );

    return $fields;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityForm().
   */
  public function entityForm($entity_form, &$form_state) {
    global $user;

    // Get the labels (box / variation).
    $labels = $this->labels();

    $box = $entity_form['#entity'];
    $extra_fields = field_info_extra_fields('commerce_box', $box->type, 'form');

    $entity_form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('Enter a name to identify this box.'),
      '#default_value' => $box->name,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#fieldset' => 'box_details',
    );

    $entity_form['status'] = array(
      '#type' => 'select',
      '#title' => t('Status'),
      '#default_value' => $box->status,
      '#options' => commerce_box_get_statuses(),
      '#required' => TRUE,
      '#fieldset' => 'box_details',
    );

    // Attach fields.
    $langcode = entity_language('commerce_box', $box);
    field_attach_form('commerce_box', $box, $entity_form, $form_state, $langcode);

    return $entity_form;
  }
}
