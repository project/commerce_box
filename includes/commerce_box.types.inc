<?php

/**
 * Menu callback: display an overview of available types.
 */
function commerce_box_types_overview() {
  $header = array(
    t('Name'),
    t('Operations'),
  );

  $rows = array();

  // Loop through all defined box types.
  foreach (commerce_box_types() as $type => $box_type) {
    // Build the operation links for the current line item type.
    $type_arg = strtr($type, '_', '-');
    $links = menu_contextual_links('commerce-box-type', 'admin/commerce/config/boxes', array($type_arg));

    // Add the line item type's row to the table's rows array.
    $rows[] = array(
      theme('commerce_box_type_admin_overview', array('box_type' => $box_type)),
      theme('links', array('links' => $links, 'attributes' => array('class' => 'links inline operations'))),
    );
  }

  // If no line item types are defined...
  if (empty($rows)) {
    // Add a standard empty row with a link to add a new line item type.
    $rows[] = array(
      array(
        'data' => t('There are no box types defined by enabled modules.'),
        'colspan' => 2,
      )
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Builds an overview of a box type for display to an administrator.
 *
 * @param $variables
 *   An array of variables used to generate the display; by default includes the
 *     type key with a value of the box type object.
 *
 * @ingroup themeable
 */
function theme_commerce_box_type_admin_overview($variables) {
  $box_type = $variables['box_type'];

  $output = check_plain($box_type['name']);
  $output .= ' <small>' . t('(Machine name: @type)', array('@type' => $box_type['type'])) . '</small>';

  if (!empty($box_type['description'])) {
    $output .= '<div class="description">' . filter_xss_admin($box_type['description']) . '</div>';
  }

  return $output;
}
