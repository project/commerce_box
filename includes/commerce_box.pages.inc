<?php

/**
 * @file
 * Provides pages for commerce box entities.
 */

/**
 * Menu callback to display a list of box types that can be created.
 */
function commerce_box_add_page() {
  $item = menu_get_item();

  $content = system_admin_menu_block($item);

  // Bypass the box type add page if only one box type is available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }

  return theme('commerce_box_add_list', array('content' => $content));
}

/**
 * Theme function for selecting the type of box entity to add.
 */
function theme_commerce_box_add_list($variables) {
  $content = $variables['content'];
  $output = '';

  if ($content) {
    $output = '<dl class="commerce-box-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    $output = '<p>' . t('There are no defined box types. The Commerce Box module provides a default box type and other modules can define their own. If you are seeing this, then someone deleted the default box type') . '</p>';
  }

  return $output;
}

/**
 * Page callback for commerce box form.
 */
function commerce_box_form_wrapper($commerce_box) {
  // Include the forms file for the commerce box module.
  module_load_include('inc', 'commerce_box', 'includes/commerce_box.forms');
  return drupal_get_form('commerce_box_form', $commerce_box);
}

/**
 * Page callback for commerce box delete form.
 */
function commerce_box_delete_form_wrapper($commerce_box) {
  // Include the form file for deleting commerce box entities.
  module_load_include('inc', 'commerce_box', 'includes/commerce_box.forms');
  return drupal_get_form('commerce_box_delete_form', $commerce_box);
}
