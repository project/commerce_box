<?php

/**
 * Views for the default commerce box UI.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_box_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'commerce_boxes';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'commerce_box';
  $view->human_name = 'Commerce Boxes';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Commerce Boxes';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer commerce boxes';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Commerce Box: Box Id */
  $handler->display->display_options['fields']['box_id']['id'] = 'box_id';
  $handler->display->display_options['fields']['box_id']['table'] = 'commerce_box';
  $handler->display->display_options['fields']['box_id']['field'] = 'box_id';
  /* Field: Commerce Box: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'commerce_box';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Commerce Box: Box Dimensions */
  $handler->display->display_options['fields']['commerce_box_dimensions']['id'] = 'commerce_box_dimensions';
  $handler->display->display_options['fields']['commerce_box_dimensions']['table'] = 'field_data_commerce_box_dimensions';
  $handler->display->display_options['fields']['commerce_box_dimensions']['field'] = 'commerce_box_dimensions';
  $handler->display->display_options['fields']['commerce_box_dimensions']['label'] = 'Dimensions';
  $handler->display->display_options['fields']['commerce_box_dimensions']['click_sort_column'] = 'length';
  $handler->display->display_options['fields']['commerce_box_dimensions']['settings'] = array(
    'field_formatter_class' => '',
  );
  /* Field: Commerce Box: Box Max Weight */
  $handler->display->display_options['fields']['commerce_box_max_weight']['id'] = 'commerce_box_max_weight';
  $handler->display->display_options['fields']['commerce_box_max_weight']['table'] = 'field_data_commerce_box_max_weight';
  $handler->display->display_options['fields']['commerce_box_max_weight']['field'] = 'commerce_box_max_weight';
  $handler->display->display_options['fields']['commerce_box_max_weight']['label'] = 'Max Weight';
  $handler->display->display_options['fields']['commerce_box_max_weight']['click_sort_column'] = 'weight';
  $handler->display->display_options['fields']['commerce_box_max_weight']['settings'] = array(
    'field_formatter_class' => '',
  );
  /* Field: Commerce Box: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'commerce_box';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  /* Field: Commerce Box: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'commerce_box';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['label'] = 'Options';
  $handler->display->display_options['fields']['operations']['add_destination'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/commerce/boxes/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Boxes';
  $handler->display->display_options['tab_options']['description'] = 'Manage boxes in the store';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';

  $views[$view->name] = $view;

  return $views;
}
