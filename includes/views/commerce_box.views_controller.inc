<?php

/**
 * @file
 * Provides Views integration.
 */

class CommerceBoxViewsController extends EntityDefaultViewsController {

  function views_data() {
    $data = parent::views_data();

    $data['commerce_box']['operations'] = array(
      'field' => array(
        'title' => t('Operations links'),
        'help' => t('Display operations links for commerce boxes'),
        'handler' => 'commerce_box_handler_field_box_operations',
      ),
    );
    return $data;
  }
}
