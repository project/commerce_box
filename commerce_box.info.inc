<?php

/**
 * @file
 * Provides entity info for commerce box entities.
 */

/**
 * Implements hook_entity_property_info().
 */
function commerce_box_entity_property_info() {
  $info = array();

  // Add meta-data about the basic properties.
  $properties = &$info['commerce_box']['properties'];

  $properties['box_id'] = array(
    'label' => t('Box Id'),
    'description' => t('The internal numeric ID of the box'),
    'type' => 'integer',
    'schema field' => 'box_id',
  );

  $properties['type'] = array(
    'label' => t('Type'),
    'description' => t('The type of box'),
    'type' => 'token',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_box entities',
    'options list' => 'commerce_box_type_options_list',
    'required' => TRUE,
    'schema field' => 'type',
  );

  $properties['language'] = array(
    'label' => t('Language'),
    'type' => 'token',
    'description' => t('The language the box was created in.'),
    'setter callback' => 'entity_property_verbatim_set',
    'options list' => 'entity_metadata_language_list',
    'schema field' => 'language',
    'setter permission' => 'administer commerce_box entities',
  );

  $properties['name'] = array(
    'label' => t('Name'),
    'description' => t('The name of the box.'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'name',
  );

   $properties['status'] = array(
    'label' => t('Status'),
    'description' => t('Tiny integer to specify the box status.'),
    'type' => 'integer',
    'options list' => 'commerce_box_status_options_list',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_box entities',
    'schema field' => 'status',
  );

  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the box was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_box entities',
    'schema field' => 'created',
  );

  $properties['changed'] = array(
    'label' => t('Date updated'),
    'description' => t('The date the box was most recently updated.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'query callback' => 'entity_metadata_table_query',
    'setter permission' => 'administer commerce_box entities',
    'schema field' => 'changed',
  );

  return $info;
}
