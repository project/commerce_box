ABOUT COMMERCE BOX
-----------------------------

Commerce box module provides a base fieldable entity for storing box data that
is used to package commerce products. The goal of this module is to provide a
base that can extend commerce shipment and provide a more robust shipping and
fulfillment system for Drupal Commerce.
